<?php


class WEBIOTSFaq{



    function __construct(){

        // File upload allowed

        $whitelist_files[]      = array("mimetype"=>"image/jpeg","ext"=>"jpg") ;
        $whitelist_files[]      = array("mimetype"=>"image/jpg","ext"=>"jpg") ;
        $whitelist_files[]      = array("mimetype"=>"image/png","ext"=>"png") ;
        $whitelist_files[]      = array("mimetype"=>"text/plain","ext"=>"txt") ;
        $this->whitelist_files = $whitelist_files;
        add_action("plugins_loaded", array($this, "webiots_textdomain")); //load language/textdomain
        /** register post type **/
        add_action("init", array($this, "post_type_webiots_faq"));



        /** register metabox for admin **/
      if(is_admin()){
        add_action("admin_head",array($this,"admin_head_webiots_Faq"),1);
      }
    }





    // Register textdomain
    function webiots_textdomain(){
        //load_plugin_textdomain("webiots-Faq", false, Faq_DIR . "/languages");
    }
    /** register post for table test **/


    /** register post for table faq **/
    public function post_type_webiots_faq()
    {
        $labels = array(
            "name" => _x("Faq", "post type general name", "webiots-Faq"),
            "singular_name" => _x("Faq", "post type singular name", "webiots-Faq"),
            "menu_name" => _x("Faq", "admin menu", "webiots-Faq"),
            "name_admin_bar" => _x("Faq", "add new on admin bar", "webiots-Faq"),
            "add_new" => _x("Add New FAQ", "item", "webiots-Faq"),
            "add_new_item" => __("Add New FAQ", "webiots-Faq"),
            "new_item" => __("new item", "webiots-Faq"),
            "edit_item" => __("Edit Faq", "webiots-Faq"),
            "view_item" => __("View Faq", "webiots-Faq"),
            "all_items" => __("All FAQs", "webiots-Faq"),
            "search_items" => __("Search FAQ", "webiots-Faq"),
            "parent_item_colon" => __("parent faq:", "webiots-Faq"),
            "not_found" => __("not found", "webiots-Faq"),
            "not_found_in_trash" => __("not found in trash", "webiots-Faq"));
        $args = array(
            "labels" => $labels,
            "public" => true,
            "menu_icon" => "dashicons-admin-comments",
            "publicly_queryable" => false,
            "show_ui" => true,
            "show_in_menu" => true,
            "query_var" => true,
            "capability_type" => "page",
            "has_archive" => true,
            "hierarchical" => true,
            "menu_position" => null,
            "taxonomies" => array(),
            "supports" => array("title"));
        register_post_type("wifaq", $args);
    }

    /** register metabox for faq **/
    public function metabox_webiots_faq($hook)
    {
        $allowed_hook = array("webiots_faq");
        if(in_array($hook, $allowed_hook))
        {
            add_meta_box("metabox_webiots_faq",
                __("faq","webiots-Faq"),
                array($this,"metabox_webiots_faq_callback"),
                $hook,
                "normal",
                "high");

        }


    }


    function wi_remove_view( $wiview ) {
        global $post;
        if( $post->post_type == 'faq_cpt' ) {
            unset( $wiview['view'] );
            unset( $wiview['inline hide-if-no-js'] );
        }
        return $wiview;
    }






}


new WEBIOTSFaq();




/*
 *  Get All the faq
 */
function shortcode_webiots_faq( $atts ) {



    $slide = $atts['slide'];//Getting Slide Templates
    global $wp_query,$post;

    $atts = shortcode_atts( array(
        'faq_name' => ''
    ), $atts );

    $loop = new WP_Query( array(
        'posts_per_page'    => -1,
        'post_type'         => 'webiots_faq',

    ) );

    if( ! $loop->have_posts() ) {
        return false;
    }
    ob_start();
    if($slide=="slider1"){
        include_once(TEAMSHOWCASE_PATH.'/templates/slider/slide1.php');
    }else if($slide=="slider2"){
        include_once(TEAMSHOWCASE_PATH.'/templates/slider/slide2.php');
    }else if($slide=="slider3"){
        include_once(TEAMSHOWCASE_PATH.'/templates/slider/slide3.php');
    }else if($slide=="slider4"){
        include_once(TEAMSHOWCASE_PATH.'/templates/slider/slide4.php');
    }else{
        include_once(TEAMSHOWCASE_PATH.'/templates/slider/slide1.php');
    }
    $output = ob_get_clean();
    //print $output; // debug
    return $output;



    wp_reset_postdata();
}
/*
 * Registering Scripts and styles
 */








//load scripts
function faq_scripts_styles() {
//Register Styles


    /*wp_register_style( 'bootstrapcss', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' );
    wp_register_style( 'googlefonts', 'https://fonts.googleapis.com/css?family=Roboto' );
    wp_register_style( 'font-awesome', 'http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' );
    wp_register_style( 'slick',plugins_url( 'assets/css/slick.css', dirname(__FILE__) ));
    wp_register_style( 'slick-theme',plugins_url( 'assets/css/slick-theme.css', dirname(__FILE__) ));
    wp_register_style( 'faq',plugins_url( 'assets/css/faq.css', dirname(__FILE__) ));
    wp_register_style("font-awesome", "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css",array(),"1.2.4" );

    wp_enqueue_style( 'bootstrapcss' );
    wp_enqueue_style( 'googlefonts' );
    wp_enqueue_style( 'font-awesome' );
    wp_enqueue_style( 'slick' );
    wp_enqueue_style( 'slick-theme' );
    wp_enqueue_style( 'faq' );
    wp_enqueue_style("font-awesome");*/
//Register Scripts
    /*  wp_register_script( 'bootstrapjs', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js',array(),'3.3.6',true);
      wp_register_script( 'slick', plugins_url( 'assets/js/slick.js', dirname(__FILE__) ),array(),'1.0',true);
      wp_register_script( 'slickfunction', plugins_url( 'assets/js/function.js', dirname(__FILE__) ),array(),'1.0',true);
      wp_enqueue_script( 'bootstrapjs' );
      wp_enqueue_script( 'slick' );
      wp_enqueue_script( 'slickfunction' );*/


}
/*
* Funtion to Insert faq From Frontend
*/


if (isset( $_POST['submit_faq'] ) )
{


    $testimonial_args = array(

        'post_title'    => $_POST['faq_name'],

        'post_content'  => $_POST['faq_description'],

        'post_status'   => 'pending',

        'post_type' => 'webiots_faq'

    );

    // insert the post into the database

    $postid = wp_insert_post( $testimonial_args, $wp_error);


    $post_faq_designation = sanitize_text_field($_POST["faq_designation"] );
    // Update the meta field.
    update_post_meta($postid, "_faq_designation", $post_faq_designation);
    // Sanitize the user input.
    $post_faq_fb_url = sanitize_text_field($_POST["faq_fb_url"] );
    // Update the meta field.
    update_post_meta($postid, "_faq_fb_url", $post_faq_fb_url);
    // Sanitize the user input.
    $post_faq_linkedin_url = sanitize_text_field($_POST["faq_linkedin_url"] );
    // Update the meta field.
    update_post_meta($postid, "_faq_linkedin_url", $post_faq_linkedin_url);
    // Sanitize the user input.
    $post_faq_twitter = sanitize_text_field($_POST["faq_twitter"] );
    // Update the meta field.
    update_post_meta($postid, "_faq_twitter", $post_faq_twitter);

    $post_faq_rate = sanitize_text_field($_POST["faq_rate"] );
    // Update the meta field.


    if (!function_exists('wp_generate_attachment_metadata')){
        require_once(ABSPATH . "wp-admin" . '/includes/image.php');
        require_once(ABSPATH . "wp-admin" . '/includes/file.php');
        require_once(ABSPATH . "wp-admin" . '/includes/media.php');
    }
    if ($_FILES) {
        foreach ($_FILES as $file => $array) {
            if ($_FILES[$file]['error'] !== UPLOAD_ERR_OK) {
                return "upload error : " . $_FILES[$file]['error'];
            }
            $attach_id = media_handle_upload( $file, $postid );
        }
    }
    if ($attach_id > 0){
        //and if you want to set that image as Post  then use:
        update_post_meta($postid,'_thumbnail_id',$attach_id);
    }


}
/*
 *   Register A category for the FAQ
 */

function faq_category() {

    register_taxonomy(
        'faq-category',
        'webiots_faq',
        array(
            'label' => __( 'FAQ Category' ),
            'rewrite' => array( 'slug' => 'faq-category' ),
            'hierarchical' => true,
        )
    );
}
add_action( 'init', 'faq_category' );

add_filter('manage_wifaq_posts_columns', 'column_head_wifaq', 10);
add_action('manage_wifaq_posts_custom_column', 'column_content_wifaq', 10, 2);

// CREATE TWO FUNCTIONS TO HANDLE THE COLUMN
function column_head_wifaq($defaults) {
    $defaults['directors_name'] = 'ShortCode';
    return $defaults;
}
function column_content_wifaq($column_name, $post_ID) {
    if ($column_name == 'directors_name') {
        // show content of 'directors_name' column

        echo '<input onClick="this.select();" value="[wifaq id='. $post_ID .']" >';
    }
}


/*-------------------------------------------------------------------------------*/
/* Lets register our shortcode
/*-------------------------------------------------------------------------------*/
function wifaq_call($atts){
    extract( shortcode_atts( array(

        'id' => null,

    ), $atts ) );
 ob_start();
   include_once(WEBIOTS_FAQ_PATH.'/templates/layout1.php');
  $output = ob_get_clean();return $output;
}
