<?php
/*
Plugin Name: Webiots FAQ
Plugin URI: https://www.webiots.com
Description: Webiots FAQ
Version: 1.0
Author: Webiots
Author URI: https://www.webiots.com
License: GPLv2 or later
Text Domain: webiots-faq
*/
define("WEBIOTS_FAQ_PATH",dirname(__FILE__));
//Include the Functions File
include('includes/functions.php');

include_once('metabox/meta-box-class/my-meta-box-class.php');
include_once('metabox/class-usage-demo.php');



if ( is_admin() ) {
    add_filter('post_row_actions', 'wi_remove_view', 10, 2);

    function hide_buttons(){
        $my_post_type = 'wifaq';
        global $post;
        if($post->post_type == $my_post_type){
            echo '
                <style type="text/css">
                    #misc-publishing-actions,
                    #minor-publishing-actions{
                        display:none;
                    }
                </style>
            ';
        }
    }
    add_action('admin_head-post.php', 'hide_buttons');
    add_action('admin_head-post-new.php', 'hide_buttons');
}

add_shortcode('wifaq','wifaq_call');