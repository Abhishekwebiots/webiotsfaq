<style type="text/css">


    #accordion {
        margin-top:10px;

    }

    #accordion div {
        background:white;
        height:100px;
        line-height:100px;
        display:none;
        border-bottom:thin solid #cecece;
        padding-left:15px;
    }

    #accordion a {
        display:block;

        background:#f4f4f4;
        background-image: -webkit-linear-gradient(white,#ededed);
        background-image: -moz-linear-gradient(white,#ededed);
        background-image: -o-linear-gradient(white,#ededed);
        background-image: -ms-linear-gradient(white,#ededed);
        background-image:linear-gradient(white,#ededed);
        color:#959696;
        padding-left:15px;
        height:40px;
        line-height:40px;
        text-decoration:none;
        border-bottom:thin solid #cecece;
        font-family:Arial;
        font-size:13px;
        font-weight:bold;
        text-shadow:0px 1px 1px white;
    }

</style>

    <div id="accordion">
    <?php
    $saved_data = get_post_meta($id,'wi_re_',true);
    echo '<dl  style="padding:10px;" class="Zebra_Accordion'.$id.'">';
    // check if question empty
    if(empty($saved_data)){echo "<h2>OOps ! You forgot to add questions and answers in this FAQ.</h2>";}
$i = 1;
    foreach ($saved_data as $arr){

        echo   "<a href=\"#$i\" class=\"first\">".$arr['wi_re_text_field_id']."</a>";
        echo" <div id='$i'>".$arr['wi_re_textarea_field_id']."</div>";
    $i++;
    }
    echo "</dl>";
    ?>
</div>
<script type="text/javascript">
    // display the first div by default.
    jQuery("#accordion div").first().css('display', 'block');


    // Get all the links.
    var link = jQuery("#accordion a");

    // On clicking of the links do something.
    link.on('click', function(e) {

        e.preventDefault();

        var a = jQuery(this).attr("href");

        jQuery(a).slideDown('fast');

        //$(a).slideToggle('fast');
        jQuery("#accordion div").not(a).slideUp('fast');

    });
</script>
